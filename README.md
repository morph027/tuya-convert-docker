# Dockerized [tuya-convert](https://github.com/ct-Open-Source/tuya-convert)

Run tuya-convert on any linux machine! ([just make sure your wifi adapter supports AP mode](https://wiki.archlinux.org/index.php/Software_access_point#Wi-Fi_device_must_support_AP_mode))

## Instructions

```bash
docker run --rm -it --network host --cap-add NET_ADMIN --init registry.gitlab.com/morph027/tuya-convert-docker:latest
```

To copy your own firmware, mount the local folder:

```bash
docker run --rm -it --network host --cap-add NET_ADMIN --init -v /your/folder/containing/firmware:/tuya-convert/files registry.gitlab.com/morph027/tuya-convert-docker:latest
```

To run on a RPI, replace `registry.gitlab.com/morph027/tuya-convert-docker:latest` by `registry.gitlab.com/morph027/tuya-convert-docker:armhf` for RPI3 or `registry.gitlab.com/morph027/tuya-convert-docker:armel` for RPI Zero.

## Build docker images

### amd64

```
docker build -t tuya-convert -f docker/amd64/Dockerfile .
```

### Cross-build for arm (RPI)

We will cross-build this on a machine with more mojo.

#### Prepare

```
sudo apt install qemu qemu-user-static qemu-user binfmt-support
docker run --rm --privileged multiarch/qemu-user-static:register
cp /usr/bin/qemu-arm-static .
```

#### armhf (for RPI 3)

```
docker build -t tuya-convert -f docker/armhf/Dockerfile .
```

#### armel (for RPI Zero)

```
docker build -t tuya-convert -f docker/armel/Dockerfile .
```
