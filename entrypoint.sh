#!/bin/bash

echo "WLAN=${WLAN:-wlan0}" > /tuya-convert/config.txt
echo "AP=${AP:-vtrust-flash}" >> /tuya-convert/config.txt
echo "GATEWAY=${GATEWAY:-10.42.42.1}" >> /tuya-convert/config.txt

exec /tuya-convert/start_flash.sh
